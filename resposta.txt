Os tempos testados foram:
    @time matrix_pot([1 2 3; 4 5 6; 7 8 9],7)=0.040099 seconds
    @time matrix_pot_by_squaring([1 2 3; 4 5 6; 7 8 9], 7)=  0.000006 seconds 

    @time matrix_pot([1 2 3; 4 5 6; 7 8 9],3)=0.039883 seconds 
    @time matrix_pot_by_squaring([1 2 3; 4 5 6; 7 8 9],7)=0.000005 seconds

    @time matrix_pot([1 2 3; 4 5 6; 7 8 9],13)=0.039960 seconds
    @time matrix_pot_by_squaring([1 2 3; 4 5 6; 7 8 9], 13)=  0.000014 seconds

    @time matrix_pot([1 2 3; 4 5 6; 7 8 9],10)=0.040019 seconds 
    @time matrix_pot_by_squaring([1 2 3; 4 5 6; 7 8 9], 10)=0.000014 seconds 

Como se pode observar acima, houve diferença nos tempos de execução das funções matrix_pot() e matrix_pot_by_squaring() para os mesmos argumentos. A matrix_pot_by_squaring() é no geral mais rápida em tempo de execução pois a utilização da potenciação pelo método dos quadrados nela diminui a número de operações de multiplicação de matrizes realizadas. Neste caso, se o expoente que se eleva a matriz for par, o número de multiplicações realizadas é igual à metade do valor do expoente. Se for ímpar, o número de multiplicações realizadas é igual ao expoente somado a um dividido por dois.
Já no caso da matrix_pot() o número de multiplicações realizadas é sempre igual ao número do expoente, o que aumenta o tempo de execução.
