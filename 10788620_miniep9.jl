#parte1
function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M,p)
    if p==1
        return M
    else
        expoente=2
        resultado=M
        while expoente<=p
            resultado=multiplica(resultado,M)
            expoente+=1
        end
        return resultado
    end
end
#parte2
function matrix_pot_by_squaring(M,e)
    if e==1
        return M
    elseif e==2
        return multiplica(M,M)
    elseif e%2==0
        return multiplica(matrix_pot_by_squaring(M,e/2),matrix_pot_by_squaring(M,e/2))
    else
        return multiplica(M,multiplica(matrix_pot_by_squaring(M,e÷2),matrix_pot_by_squaring(M,e÷2)))
    end
end

using Test
function testeparte1()
    @test matrix_pot([1 2 ; 3 4], 1)==[1 2;3 4]
    @test matrix_pot([1 2 ; 3 4], 2)==[7.0 10.0; 15.0 22.0]
    @test matrix_pot([1 2 ; 3 4], 3)==[37 54;81 118]
    @test isapprox(matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7), [5.64284e8 4.70889e8 3.23583e8 3.51859e8;
8.31242e8 6.93529e8 4.76619e8 5.18192e8;
5.77004e8 4.81473e8 3.30793e8 3.59677e8;
7.99037e8 6.66708e8 4.58121e8 4.98127e8], atol=5000)
end

function testeparte2()
    @test matrix_pot_by_squaring([1 2 ; 3 4], 1)==[1 2;3 4]
    @test matrix_pot_by_squaring([1 2 ; 3 4], 2)==[7.0 10.0; 15.0 22.0]
    @test matrix_pot_by_squaring([1 2 ; 3 4], 3)==[37 54;81 118]
    @test isapprox(matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7), [5.64284e8 4.70889e8 3.23583e8 3.51859e8;
8.31242e8 6.93529e8 4.76619e8 5.18192e8;
5.77004e8 4.81473e8 3.30793e8 3.59677e8;
7.99037e8 6.66708e8 4.58121e8 4.98127e8], atol=5000)
end
